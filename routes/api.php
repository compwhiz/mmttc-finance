<?php

use Illuminate\Http\Request;

Route::resource('course', 'CourseController');
Route::resource('students', 'StudentController');
Route::resource('terms', 'TermController');
Route::resource('payments', 'PaymentController');
Route::post('getStudentsbyTerminCourse', 'CourseController@getStudentsbyTerminCourse');

Route::get('unRegisteredTermStudents/{termID}', function ($termID) {

    $students = \App\Student::whereDoesntHave('term', function ($query) use ($termID) {
        $query->where('term_id', $termID);
    })->get();
    return response()->json($students->load('courses'));
});

Route::post('registerStudents', 'StudentController@registerStudents');
Route::post('makeboarder', function (Request $request) {

    DB::table('student_term')
        ->where('student_id', $request->input('student'))
        ->where('term_id', $request->input('term'))
    ->update([
        'fee' => DB::raw('fee + 15000'),
        'boarder' => 1
    ]);


});

Route::post('transfermoney', function (Request $request) {

    $transfer = new \App\Feetransfer();

    $transfer->amount = $request->amount;
    $transfer->date = $request->date;
    $transfer->transferredto = $request->transferredto;
    $transfer->term_id = $request->input('term.id');
    $transfer->student_id = $request->student;
    $transfer->saveOrFail();

    return response()->json($transfer);
});
