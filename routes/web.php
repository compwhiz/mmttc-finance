<?php

Route::get('install', function () {
    Artisan::call('migrate');

    return Artisan::output();
});

Route::resource('subjectgroups', 'SubjectgroupController');

Route::get('/{any}', 'HomeController@index')->where('any', '.*');

Auth::routes();

