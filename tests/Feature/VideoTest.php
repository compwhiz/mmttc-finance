<?php

namespace Tests\Feature;

use http\Client\Curl\User;
use PhpParser\Node\VarLikeIdentifier;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class VideoTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_an_Admin_can_Create_Video_Lectures()
    {
        $user = factory(\App\User::class)->create([
            'role' => 'admin'
        ]);

        $videoSeries = $user->lecture()->save($series = factory(Series::class)->make());

        $this->actingAs($user)->json('GET','/lectures');

        $user->purchase($videoSeries);
        // Go to a video test

        // Purchase the test

        // View the test
        $response = $this->get('/');

        $response->assertStatus(200);
    }
}
