<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Stream;
use Faker\Generator as Faker;

$factory->define(Stream::class, function (Faker $faker) {
    return [
        'form' => $faker->numberBetween(1,4),
        'stream'=> $faker->randomDigit
    ];
});
