<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Student;
use Faker\Generator as Faker;

$factory->define(Student::class, function (Faker $faker) {
    return [
        'fullname'      => $faker->name,
        'admno'         => $faker->randomNumber(4),
        'stream'        => $faker->numberBetween(1,4),
        'form'        => $faker->numberBetween(1,4),
        'upi'           => $faker->creditCardNumber,
        'gender'        => $faker->randomElement(array('M','F')),
        'kcpescore'     => $faker->randomNumber(3),
        'guardianphone' => $faker->phoneNumber,
        'guardianemail' => $faker->email,
        'indexnumber'   => $faker->swiftBicNumber,
    ];
});
