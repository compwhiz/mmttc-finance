<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $form1A = factory(\App\Stream::class)->create([
            'stream' => 'A',
            'form' => '1',
        ]);
        factory(\App\Student::class,10)->create([
            'stream' => $form1A['id'],
            'form' => $form1A['form']
        ]);
        $form1B = factory(\App\Stream::class)->create([
            'stream' => 'B',
            'form' => '1',
        ]);
        factory(\App\Student::class,10)->create([
            'stream' => $form1B['id'],
            'form' => $form1B['form']
        ]);

        $form2A = factory(\App\Stream::class)->create([
            'stream' => 'A',
            'form' => '2',
        ]);
        factory(\App\Student::class,10)->create([
            'stream' => $form2A['id'],
            'form' => $form2A['form']
        ]);

        $form2B = factory(\App\Stream::class)->create([

                'stream' => 'B',
                'form' => '2',

        ]);
        factory(\App\Student::class,10)->create([
            'stream' => $form2B['id'],
            'form' => $form2B['form']
        ]);

        $form3A = factory(\App\Stream::class)->create([
            'stream' => 'A',
            'form' => '3',
        ]);
        factory(\App\Student::class,10)->create([
            'stream' => $form3A['id'],
            'form' => $form3A['form']
        ]);

        $form3B = factory(\App\Stream::class)->create([
            'stream' => 'B',
            'form' => '3',
        ]);
        factory(\App\Student::class,10)->create([
            'stream' => $form3B['id'],
            'form' => $form3B['form']
        ]);

        $form4A = factory(\App\Stream::class)->create([
            'stream' => 'A',
            'form' => '4',
        ]);
        factory(\App\Student::class,10)->create([
            'stream' => $form4A['id'],
            'form' => $form4A['form']
        ]);

        $form4B = factory(\App\Stream::class)->create([
            'stream' => 'B',
            'form' => '4',
        ]);

        factory(\App\Student::class,10)->create([
            'stream' => $form4B['id'],
            'form' => $form4B['form']
        ]);

        factory(\App\Teacher::class,5)->create();

    }
}
