@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-xl-12">
            <div class="hk-row">
                <div class="col-lg-4">
                    <div class="card card-sm border-bottom-0">
                        <div class="card-header card-header-action">
                            <h6>Moi Girls Secondary School</h6>
                            <div class="d-flex align-items-center card-action-wrap">
                                <div class="inline-block dropdown">
                                    <a class="dropdown-toggle no-caret" data-toggle="dropdown" href="#"
                                       aria-expanded="false" role="button"><i class="ion ion-ios-more"></i></a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a class="dropdown-item" href="#">Action</a>
                                        <a class="dropdown-item" href="#">Another action</a>
                                        <a class="dropdown-item" href="#">Something else here</a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="#">Separated link</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body pa-0">
                            <div class="pa-15">
                                <div class="row">
                                    <div class="col-3">
                                        <span class="d-block text-capitalize">Form 1</span>
                                        <span class="d-block text-dark font-weight-500 font-20">100</span>
                                    </div>
                                    <div class="col-3">
                                        <span class="d-block text-capitalize">Form 2</span>
                                        <span class="d-block text-dark font-weight-500 font-20">150</span>
                                    </div>
                                    <div class="col-3">
                                        <span class="d-block text-capitalize">Form 3</span>
                                        <span class="d-block text-dark font-weight-500 font-20">132</span>
                                    </div>
                                    <div class="col-3">
                                        <span class="d-block text-capitalize">Form 4</span>
                                        <span class="d-block text-dark font-weight-500 font-20">154</span>
                                    </div>
                                </div>
                            </div>
                            <div class="progress-wrap">
                                <div class="progress rounded-bottom-left rounded-bottom-right">
                                    <div class="progress-bar bg-primary w-15" role="progressbar" aria-valuenow="15"
                                         aria-valuemin="0" aria-valuemax="100"></div>
                                    <div class="progress-bar bg-indigo-light-1 w-35" role="progressbar"
                                         aria-valuenow="35" aria-valuemin="0" aria-valuemax="100"></div>
                                    <div class="progress-bar bg-indigo-light-2 w-50" role="progressbar"
                                         aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-8">
                    <div class="card-group hk-dash-type-2">
                        <div class="card card-sm">
                            <div class="card-body">
                                <span class="d-block font-14 font-weight-500 text-dark">Students</span>
                                <div class="d-flex align-items-center justify-content-between">
                                    <div class="display-5 font-weight-400 text-dark">690</div>
                                    <div class="font-13 font-weight-500">
                                    </div>
                                </div>
                                <div class="mt-20">
                                    <div id="sparkline_1"></div>
                                </div>
                            </div>
                        </div>
                        <div class="card card-sm">
                            <div class="card-body">
                                <span class="d-block font-14 font-weight-500 text-dark">Teachers</span>
                                <div class="d-flex align-items-center justify-content-between">
                                    <div class="display-5 font-weight-400 text-dark">30</div>
                                    <div class="font-13 font-weight-500">
                                    </div>
                                </div>
                                <div class="mt-20">
                                    <div id="sparkline_2"></div>
                                </div>
                            </div>
                        </div>
                        <div class="card card-sm">
                            <div class="card-body">
                                <span class="d-block font-14 font-weight-500 text-dark">Streams</span>
                                <div class="d-flex align-items-center justify-content-between">
                                    <div class="display-5 font-weight-400 text-dark">12</div>
                                    <div class="font-13 font-weight-500"></div>
                                </div>
                                <div class="mt-20">
                                    <div id="sparkline_3"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
