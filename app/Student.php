<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Student
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Student newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Student newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Student query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Student whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Student whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Student whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string $fullname
 * @property string $admno
 * @property string $stream
 * @property string|null $upi
 * @property string|null $gender
 * @property string|null $kcpescore
 * @property string|null $guardianphone
 * @property string|null $guardianemail
 * @property string|null $indexnumber
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Student whereAdmno($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Student whereFullname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Student whereGender($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Student whereGuardianemail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Student whereGuardianphone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Student whereIndexnumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Student whereKcpescore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Student whereStream($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Student whereUpi($value)
 */
class Student extends Model
{
//    protected $casts = ['boarder'=>'boolean'];
    public function courses()
    {
        return $this->hasOne(Course::class,'id','course');
    }

    public function term()
    {
        return $this->belongsToMany(Term::class)->withPivot(['fee','boarder']);
    }

    public function payments()
    {
        return $this->hasMany(Payment::class,'student_id','id');
    }
    public function custompayments()
    {
        return $this->hasMany(Payment::class,'student_id','student_id');
    }
}
