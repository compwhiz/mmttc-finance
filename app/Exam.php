<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exam extends Model
{
    public function examConfigurations()
    {
        return $this->hasOne(Examconfig::class);
    }
}
