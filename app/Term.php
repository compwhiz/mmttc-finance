<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Term extends Model
{
    public function payments()
    {
        return $this->hasMany(Payment::class,'term_id','id');
    }

    public function students()
    {
        return $this->belongsToMany(Term::class)->withPivot('fee');
    }

    public function transfers()
    {
        return $this->hasMany(Feetransfer::class,'term_id','id');
    }

}
