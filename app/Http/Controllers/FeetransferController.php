<?php

namespace App\Http\Controllers;

use App\Feetransfer;
use Illuminate\Http\Request;

class FeetransferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Feetransfer  $feetransfer
     * @return \Illuminate\Http\Response
     */
    public function show(Feetransfer $feetransfer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Feetransfer  $feetransfer
     * @return \Illuminate\Http\Response
     */
    public function edit(Feetransfer $feetransfer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Feetransfer  $feetransfer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Feetransfer $feetransfer)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Feetransfer  $feetransfer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Feetransfer $feetransfer)
    {
        //
    }
}
