<?php

namespace App\Http\Controllers;

use App\Exam;
use App\Examconfig;
use App\Http\Requests\StoreExam;
use Illuminate\Http\Request;

class ExamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(Exam::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreExam $request, Exam $exam)
    {
        $exam->year = $request->year;
        $exam->term = $request->term;
        $exam->form = $request->form;
        $exam->examname = $request->examname;
        $exam->saveOrFail();
        $configuration = new Examconfig([
            'outof'         => 100,
            'contributions' => 100,
        ]);
        $exam->examConfigurations()->save($configuration);
        return response($exam);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Exam  $exam
     * @return \Illuminate\Http\Response
     */
    public function show(Exam $exam)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Exam  $exam
     * @return \Illuminate\Http\Response
     */
    public function edit(Exam $exam)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Exam  $exam
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Exam $exam)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Exam  $exam
     * @return \Illuminate\Http\Response
     */
    public function destroy(Exam $exam)
    {
        //
    }

    public function getExams(Request $request)
    {
        $returnedExams = Exam::with('examConfigurations')->where('form',$request->form)
        ->where('term',$request->term)
        ->where('year',$request->year)
        ->get();
        return response($returnedExams);
    }
}
