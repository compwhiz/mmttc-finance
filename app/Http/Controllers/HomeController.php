<?php

namespace App\Http\Controllers;

use App\Stream;
use App\Student;
use App\Teacher;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('welcome');
    }

    /**
     * @param Request $request
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'registrationcode'=> ['required'],
            'schoolname'=> ['required'],
            'moecode'=> ['required'],
            'tsccode'=> ['required'],
            'kneccode'=> ['required'],
            'mission'=> ['required'],
            'vision'=> ['required'],
            'motto'=> ['required'],
            'address'=> ['required'],
            'schoolmobile'=> ['required'],
            'website'=> ['required'],
            'email'=> ['required'],
            'county'=> ['required'],
            'constituency'=> ['required'],
            'district'=> ['required'],
            'location'=> ['required'],
            'sublocation'=> ['required'],
        ]);
    }

    public function dashboard()
    {
        return response()->json([
            'students' => $this->students(),
            'teachers' => $this->teachers(),
            'streams' => $this->streams(),

        ]);
    }

    private function students()
    {
        return Student::all()->count();
    }

    private function teachers()
    {
        return Teacher::all()->count();
    }

    private function streams()
    {
        return Stream::all()->count();

    }

    public function form()
    {

        $forms = Stream::all()->groupBy('form');
        $formCount =collect($forms)->mapWithKeys(function ($form, $key) {
            return [
                $key => collect(\App\Student::all()->whereIn('stream', $form->pluck('id')))->count(),
            ];
        });
        return response($formCount);
    }
}
