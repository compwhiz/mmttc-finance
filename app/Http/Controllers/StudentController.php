<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreStudent;
use App\Student;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(Student::with('courses')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * @throws \Throwable
     */
    public function store(StoreStudent $request, Student $student)
    {
        $student->admno = $request->admno;
        $student->fullname = $request->name;
        $student->boarder = $request->boarder;
        $student->course = $request->course;
        $student->saveOrFail();

        return response($student);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function show(Student $student)
    {
        $k = $this->getStudentWithPaymentandTerm($student);

        $paid = \DB::table('feetransfers')->where('student_id', $student->id)->sum('amount');

        $totaltoPay = $k->term->sum('pivot.fee');

        $alreadyPaid = collect($k->term)->map(function ($betst) {
            return $betst->payments->sum('amount');
        })->sum();

        return response()->json(collect($k)->merge(['expectedFee' => $totaltoPay, 'paidAlready' => $alreadyPaid,
            'paidtransfers'=>$paid]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function edit(Student $student)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Student $student)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function destroy(Student $student)
    {
        //
    }

    public function getStudentsByForm(Student $student, $form)
    {
        return response()->json($student::where('form', $form)->with('streams')->get());
    }

    public function getStudentsByStream(Student $student, $stream)
    {
        return response()->json($student::whereHas('streams', function ($query) use ($stream) {
            $query->whereId($stream);
        })->with('streams')->get());
    }

    public function getStudentsByFormAndStream(Student $student, $form)
    {

        $students = collect($student::where('form', $form)->with('streams')->get());
        $new = $students->groupBy(function ($student) {
            return $student['streams']['stream'];
        });
        return response()->json($new);


    }

    public function registerStudents(Request $request)
    {
        foreach ($request->mutis as $studenttoadd) {
            $student = Student::find($studenttoadd)->load('courses');
            if ($student->boarder === '1') {
                $feetoPay = $student->courses->fee + 15000;

            } else {
                $feetoPay = $student->courses->fee;

            }
            $student->term()->attach($request->id, [
                'fee'     => $feetoPay,
                'boarder' => $student->boarder,
            ]);
        }

        return response()->json("Done");

    }

    /**
     * @param  Student  $student
     * @return Student|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function getStudentWithPaymentandTerm(Student $student)
    {

        $constrainShifts = function ($query) use ($student) {
            $query->where('student_id', '=', $student->id);
        };

        $k = \App\Student::
        withCount('term')->with([
            'courses',
            'term'          => function ($order) {
                $order->withCount(['transfers'=> function ($quer) {
                    $quer->select(\DB::raw('SUM(amount)'));
                }]);
                $order->orderBy('year');
            },
            'term.transfers',
            'term.payments' => $constrainShifts
        ])
            ->where('id', '=', $student->id)
            ->first();

        return $k;
    }
}
