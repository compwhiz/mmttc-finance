<?php

namespace App\Http\Controllers;

use App\Examconfig;
use Illuminate\Http\Request;

class ExamconfigController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Examconfig  $examconfig
     * @return \Illuminate\Http\Response
     */
    public function show(Examconfig $examconfig)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Examconfig  $examconfig
     * @return \Illuminate\Http\Response
     */
    public function edit(Examconfig $examconfig)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Examconfig  $examconfig
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $examconfig)
    {
        return $request->all();
        $examconfig = Examconfig::find($examconfig);
        $examconfig->outof = $request->outof;
        $examconfig->contributions = $request->contribution;
        $examconfig->update();

        return response($examconfig);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Examconfig  $examconfig
     * @return \Illuminate\Http\Response
     */
    public function destroy(Examconfig $examconfig)
    {
        //
    }
}
