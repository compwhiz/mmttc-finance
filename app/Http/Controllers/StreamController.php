<?php

namespace App\Http\Controllers;

use App\Stream;
use Illuminate\Http\Request;

class StreamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     * @throws \Throwable
     */
    public function store(Request $request, Stream $stream)
    {
        $this->validate($request, [
            'form'   => ['required'],
            'stream' => ['required'],
        ]);

        [
            ['0 - 7000' => '10,000/', '45Days', '1.43'],
            ['7,000 - 10000' => '14,000', '45Days', '1.40'],
            ['10,000 - 20000' => '27,000', '45Days', '1.35'],
            ['20001 - 50,000' => '65,000', '45Days', '1.30'],
            ['50,000 - 100,000' => '120,000', '45Days', '1.20'],
            ['100,000 - 500,000' => '480,000', '45Days', '1.96'],
        ];

        //

        $stream->form = $request->form;
        $stream->stream = $request->stream;
        $stream->saveOrFail();
        exec();
        return response($stream);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Stream  $stream
     * @return \Illuminate\Http\Response
     */
    public function show(Stream $stream)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Stream  $stream
     * @return \Illuminate\Http\Response
     */
    public function edit(Stream $stream)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Stream  $stream
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Stream $stream)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Stream  $stream
     * @return \Illuminate\Http\Response
     */
    public function destroy(Stream $stream)
    {
        //
    }
}
