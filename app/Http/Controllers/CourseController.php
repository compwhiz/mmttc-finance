<?php

namespace App\Http\Controllers;

use App\Course;
use App\Http\Requests\StoreCourseRequest;
use App\Student;
use App\Term;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response(Course::withCount('students')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * @throws \Throwable
     */
    public function store(StoreCourseRequest $request, Course $course)
    {
        $course->name = $request->name;
        $course->type = $request->type;
        $course->fee = $request->fee;
        $course->saveOrFail();

        return response()->json($course);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function show(Course $course)
    {
        return response()->json($course->load('students'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function edit(Course $course)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Course $course)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function destroy(Course $course)
    {
        //
    }

    public function getStudentsbyTerminCourse(Request $request)
    {
        $latestPosts = \DB::table('payments')
            ->select('student_id', 'term_id', DB::raw('SUM(amount) as payments'))
            ->where('term_id',$request->year)
            ->groupBy('student_id', 'term_id');

        $students =Student::with('custompayments')
            ->join('student_term', 'students.id', '=', 'student_term.student_id')
//            ->joinSub($latestPosts, 'pay','students.id','pay.student_id')
            ->joinSub($latestPosts, 'pay', function ($join) use ($request) {
                $join->on('students.id', '=', 'pay.student_id')->where('pay.term_id', $request->year);
            })
            ->where('student_term.term_id', $request->year)
            ->where('students.course', $request->course)
            ->get();
        return response($students);
    }
}
