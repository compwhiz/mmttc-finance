<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{

    public function getOfferedAttribute($value)
    {
        if ($value === 0) {
            return "Not Offered";
        }
        return "Offered";
    }
    public function subjectcategory()
    {
        return $this->hasOne(Subjectgroup::class, 'id', 'category');
    }
}
